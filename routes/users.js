var express = require('express');
var router = express.Router();
var csrf = require('csurf');
var passport = require('passport');
var csrfProtection = csrf();
router.use(csrfProtection);

var User = require('../models/user');

router.use('/', function(req, res, next) {
    next();
});

/** Post to passport strategy used for creating a user from within the app. */
router.post('/add', isWillAdmin, passport.authenticate('local.signup', {
    successRedirect: '/user/settings',
    failureRedirect: '/user/settings',
    failureFlash: true
}));

/** GET request handler for /user/signin page. */
router.get('/signin', notLoggedIn, function(req, res) {
    var messages = req.flash('error');
    res.render('user/signin', {
        csrfToken: req.csrfToken(),
        title: 'Sign In',
        messages: messages,
        hasErrors: messages.length > 0
    });
});

/** POST: user signin authenticates with the local strategy. */
router.post('/signin', notLoggedIn, passport.authenticate('local.signin', {
    successRedirect: '/',
    failureRedirect: '/user/signin',
    failureFlash: true
}));

/** Route to settings overview page. */
router.get('/settings', isWillAdmin, function(req, res) {
    var messages = req.flash('error');
    User.find(function(err, docs) {
        res.render('settings/settings', {
            csrfToken: req.csrfToken(),
            title: 'Settings',
            users: docs,
            messages: messages,
            hasErrors: messages.length > 0,
            user: req.user
        });
    });
});

module.exports = router;

/** Check to make sure the user is not logged in.
 * Stops a user from viewing a signin page while signed in.
 */
function notLoggedIn(req, res, next) {
    if (!req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function isWillAdmin(req, res, next) {
    var u = req.user.common.name;
    if (req.isAuthenticated() && u == "will") {
        return next();
    }
    res.redirect('/');
}