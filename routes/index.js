var express = require('express');
var router = express.Router();
var passport = require('passport');
require('express-session');

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

var User = require('../models/user');
var Game = require('../models/game');
var Score = require('../models/score');
var Feedback = require('../models/feedback');

var configAuth = require('../config/auth');

router.get('/', function(req, res) {
    Game.find({}, null, {
        sort: {
            added: -1
        }
    }, function(err, docs) {
        res.render('dashboard', {
            title: 'Dashboard',
            game: docs,
            user: req.user
        });
    });
});

router.get('/home', function(req, res) {
    res.redirect('/');
});

router.get('/index', function(req, res) {
    res.redirect('/');
});

router.get('/game/:version', function(req, res) {
    Game.findOne({
        version: req.params.version
    }, function(err, docs) {
        res.render('games/game', {
            title: req.params.version,
            game: docs,
            user: req.user
        });
    });
});

router.get('/form/:version', function(req, res) {
    res.render('feedback/form', {
        title: req.params.version,
        user: req.user
    });
});

router.post('/form/delete/:id', canViewFeedback, function(req, res) {
    Feedback.findByIdAndRemove({ _id: req.params.id }, function(err) {
        if (err) {
            res.sendStatus(503);
        }
        res.sendStatus(200);
    })
});

router.post('/form/post/:version', function(req, res) {
    var version = req.params.version;

    var answers = [];
    var form = new Feedback();
    form.version = version;

    if (req.body.controls) {
        answers[0] = (req.body.controls);
    }
    if (req.body.art) {
        answers[1] = (req.body.art);
    }
    if (req.body.layout) {
        answers[2] = (req.body.layout);
    }
    if (req.body.sound) {
        answers[3] = (req.body.sound);
    }
    if (req.body.flow) {
        answers[4] = (req.body.flow);
    }
    if (req.body.bugs) {
        answers[5] = (req.body.bugs);
    }

    form.answers = answers;
    form.save(function(err) {
        if (err) {
            res.sendStatus(503);
        }
        res.sendStatus(200);
    });
});

router.get('/feedback', canViewFeedback, function(req, res) {
    Feedback.find(function(err, docs) {
        if (err) {
            res.render('error');
        } else {
            res.render('feedback/feedback', {
                title: 'Feedback',
                form: docs,
                user: req.user
            });
        }
    });
});

router.get('/scoreboard', function(req, res) {
    Score.find({}, null, {
        sort: {
            time: 1
        }
    }, function(err, docs) {
        if (err) {
            res.render('error');
        } else {
            res.render('scoreboard/scoreboard', {
                title: 'Scoreboard',
                score: docs,
                user: req.user
            });
        }
    });
});

router.post('/scoreboard/post/:name/:time', function(req, res) {
    new Score({
        name: req.params.name,
        time: req.params.time,
        date: Date.now()
    }).save(function(err) {
        if (err) {
            res.sendStatus(503);
        } else {
            res.sendStatus(200);
        }
    });
});

router.get('/games', isLoggedIn, function(req, res) {
    Game.find({}, null, {
        sort: {
            added: -1
        }
    }, function(err, docs) {
        if (err) {
            res.render("error", {
                title: 'Error!',
                error: error,
                user: req.user
            });
        } else {
            res.render("games/list", {
                title: 'Games List',
                game: docs,
                user: req.user
            });
        }
    });
});

/** Route to add a NEW game. Only sends status codes back.*/
router.post('/games/save', isLoggedIn, function(req, res) {
    new Game({
        version: req.body.game_version,
        description: req.body.game_description,
        added: Date.now()
    }).save(function(err) {
        if (err) {
            res.sendStatus(503);
        } else {
            res.sendStatus(200);
        }
    });
});

router.post('/games/save/:id', isLoggedIn, function(req, res) {
    var cid = req.params.id;
    Game.findByIdAndUpdate({ _id: cid }, {
            version: req.body.game_version,
            description: req.body.game_description,
            added: Date.now()
        }, {
            new: true
        },
        function(err) {
            if (err) {
                res.sendStatus(503);
            } else {
                res.sendStatus(200);
            }
        });
});

router.post('/games/delete/:id', isLoggedIn, function(req, res) {
    var cid = req.params.id;
    Game.findOneAndRemove({ _id: cid }, function(err) {
        if (err) {
            res.sendStatus(503);
        } else {
            res.sendStatus(200);
        }
    });
});

router.post('/usr/add', function(req, res) {
    'use strict';
    var username = req.body.username;
    var password = req.body.password;

    req.checkBody('username',
            'Username has to be more than 4 characters long')
        .notEmpty().isLength({ min: 4 });
    req.checkBody('password',
            'Password has to be more than 4 characters long')
        .notEmpty().isLength({ min: 4 });
    var errors = req.validationErrors();

    if (errors) {
        var messages = [];
        errors.forEach(function(error) {
            messages.push(error.msg);
        });
        return res.sendStatus(411);
    }

    User.findOne({ 'common.name': username.toLowerCase() }, function(err, user) {
        if (err) {
            return res.sendStatus(503);;
        }
        if (user) {
            return res.sendStatus(409);
        }
        var newUser = new User();
        newUser.common.name = username.toLowerCase();
        newUser.local.password = newUser.encryptPassword(password);
        newUser.save(function(err) {
            if (err) {
                return res.sendStatus(404);
            }
            return res.sendStatus(200);
        });
    });
});

router.post('/usr/delete/:id', function(req, res) {
    var cid = req.params.id;
    User.findOneAndRemove({ _id: cid }, function(err) {
        if (err) {
            res.sendStatus(503);
        } else {
            res.sendStatus(200);
        }
    });
});

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/user/signin');
});

module.exports = router;

/* HELPER FUNCTIONS */

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/user/signin');
}

function canViewFeedback(req, res, next) {
    var u = req.user.common.name;
    if (req.isAuthenticated() && (u == "will")) {
        return next();
    }
    res.redirect('/');
}