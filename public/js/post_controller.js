'use strict';
/** Created by Will Shaw */
$(document).ready(function() {

    var notify = $('.alert').eq(0);
    var msg = notify.children().eq(0);
    $('.user-delete').eq(0).hide();
    notify.hide();

    /* Flashes success or error, then updates the page. */
    function showResult(err) {
        notify.hide();
        if (err) {
            notify.removeClass('alert-success').addClass('alert-danger');
        }
        notify.slideDown(200);
        setTimeout(function() {
            notify.slideUp(200, location.reload());
        }, 2000);
    }

    /* Post data to server, and react to returned status codes. */
    function post(url, form) {
        $.ajax({
            type: 'POST',
            url: url,
            data: form,
            statusCode: {
                200: function(data) {
                    msg.html('<strong>Success!</strong>');
                    showResult();
                },
                401: function() {
                    msg.html('<strong>Error!</strong> Unauthorized.');
                    showResult(1);
                },
                404: function() {
                    msg.html('<strong>Error!</strong> File not found.');
                    showResult(1);
                },
                409: function() {
                    msg.html('<strong>Error!</strong> This user exists.');
                    showResult(1);
                },
                411: function() {
                    msg.html('<strong>Error!</strong> User/Password must be more the 4 characters.');
                    showResult(1);
                },
                503: function() {
                    msg.html('<strong>Error!</strong> There was a problem saving that to the database.');
                    showResult(1);
                }
            }
        });
    }

    $('.user-delete').on('click', function(e) {
        e.preventDefault();
        post('/usr/delete/' + $(this).prop('value'));
    });

    $('#create-btn').on('click', function(e) {
        e.preventDefault();
        post('/usr/add', $('#new-user').serialize());
    });

    /* Save the game. */
    $('#game_save').on('click', function(e) {
        e.preventDefault();
        post('/games/save', $("#new-game").serialize());
    });

    /* Delete the game. */
    $('.game_delete').on('click', function(e) {
        e.preventDefault();
        post('/games/delete/' + $(this).prop('value'));
    });

    $('.game_update').on('click', function(e) {
        e.preventDefault();
        post('/games/save/' + $(this).prop('value'), $(this).parents().eq(2).serialize());
    });
});