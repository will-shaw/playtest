module.exports = {

    /* Database connection details. 
        databaseURL: location of mongo service.
        databaseName: Name of database "schema".*/
    'database': {
        'databaseURL': 'localhost:27017/',
        'databaseName': 'playtest'
    },

    /* Graph settings */
    'graphing': {
        'fill': true
    },

    /* Node setup options */
    'device': {
        'autoResetAfter': 24,
        'updateDelay': 10000
    }

};