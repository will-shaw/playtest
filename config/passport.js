var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

/* Gets User schema and Configuration information. */
var User = require('../models/user');
var configAuth = require('./auth');

/** Enables storing user into the session. */
passport.serializeUser(function(user, done) {
    'use strict';
    done(null, user.id);
});

/** Gets the user from the session. */
passport.deserializeUser(function(id, done) {
    'use strict';
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

/** Method to create a user from within the application. */
passport.use('local.signup', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, function(req, username, password, done) {
    'use strict';
    req.checkBody('username',
            'Username has to be more than 4 characters long')
        .notEmpty().isLength({ min: 4 });
    req.checkBody('password',
            'Password has to be more than 4 characters long')
        .notEmpty().isLength({ min: 4 });
    var errors = req.validationErrors();

    if (errors) {
        var messages = [];
        errors.forEach(function(error) {
            messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }

    User.findOne({ 'common.name': username.toLowerCase() }, function(err, user) {
        if (err) {
            return done(err);
        }
        if (user) {
            return done(null, false, {
                message: 'Username is already in use'
            });
        }
        var newUser = new User();
        newUser.common.name = username.toLowerCase();
        newUser.local.password = newUser.encryptPassword(password);
        newUser.save(function(err) {
            if (err) {
                return done(err);
            }
            return done();
        });
    });
}));


/** Method for signing a user in using local credentials. */
passport.use('local.signin', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, function(req, username, password, done) {
    'use strict';
    req.checkBody('username', 'Invalid username!').notEmpty();
    req.checkBody('password', 'Invalid password!').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        var messages = [];
        errors.forEach(function(error) {
            messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }
    User.findOne({ 'common.name': username.toLowerCase() }, function(err, user) {
        if (err) {
            return done(err);
        }
        if (!user) {
            return done(null, false, { message: 'No user found' });
        }
        if (!user.validPassword(password)) {
            return done(null, false, { message: 'Wrong password' });
        }
        return done(null, user);
    });

}));