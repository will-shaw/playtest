/**
 * Feedback schema.
 * ©Will Shaw 2017
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({

    version: { type: String, required: true },

    answers: [String],

    enjoyed: Boolean

    // collection enforces consistent name in MongoDB.
}, { collection: 'Feedback' }, { 'toJSON': { getters: true, virtuals: false } }, { 'toObject': { getters: true, virtuals: false } });

module.exports = mongoose.model('Feedback', schema);