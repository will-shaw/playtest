/**
 * User Schema.
 * ©Will Shaw 2017
 */

var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var Schema = mongoose.Schema;

var schema = new Schema({

    common: {
        name: { type: String, required: true },
        email: String,
        phone: String
    },

    // Local login details.
    local: {
        password: String,
        name: String
    },

    // Slack login details.
    slack: {
        userID: String,
        accessToken: String,
        teamID: String,
        teamName: String,
        teamDomain: String
    },

    // Roles the user may have.
    role: {
        type: String,
        default: 'user',
        in: ['user', 'admin']
    }

    // Enforce consistent table name in MongoDB.
}, { collection: 'User' });

/** Used to store encrypted local passwords. */
schema.methods.encryptPassword = function(password) {
    'use strict';
    return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
};

/** Used to compare encrypted local passwords. */
schema.methods.validPassword = function(password) {
    'use strict';
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', schema);