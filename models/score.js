/**
 * Score schema.
 * ©Will Shaw 2017
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({

    name: String,

    time: Number,

    date: Date

    // collection enforces consistent name in MongoDB.
}, { collection: 'Score' }, { 'toJSON': { getters: true, virtuals: false } }, { 'toObject': { getters: true, virtuals: false } });

module.exports = mongoose.model('Score', schema);