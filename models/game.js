/** 
 * Game schema.
 * ©Will Shaw 2017
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({

    version: { type: String, required: true, unique: true },

    description: { type: String, required: true },

    added: { type: Date, default: Date.now() }

}, { collection: 'Game' });

module.exports = mongoose.model('Game', schema);